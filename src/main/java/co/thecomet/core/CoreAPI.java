package co.thecomet.core;

import co.thecomet.common.async.Async;
import co.thecomet.core.db.CoreDataAPI;
import co.thecomet.core.db.entities.GlobalModerationData;
import co.thecomet.core.db.entities.ModerationEntry;
import co.thecomet.core.db.entities.RankData;
import co.thecomet.core.db.entities.User;
import co.thecomet.core.luckyblocks.LuckyBlocks;
import co.thecomet.core.module.ModuleManager;
import co.thecomet.core.permission.commands.PermissionCommands;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.core.transaction.Transaction;
import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.mongodb.morphia.dao.BasicDAO;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CoreAPI {
    private static CoreAPI instance;
    private static ExecutorService executorService = Executors.newFixedThreadPool(10);
    private static CorePlugin plugin;
    private static GlobalModerationData globalModerationData;
    private static CoreModule core;
    private static LuckyBlocks luckyBlocks;
    private static boolean loaded;

    public CoreAPI(CorePlugin p) {
        if (instance == null) {
            instance = this;
            plugin = p;

            // Register Database Entities
            plugin.registerDBClasses(GlobalModerationData.class, ModerationEntry.class, User.class, RankData.class, Transaction.class);

            // Setup Permissions
            globalModerationData = CoreDataAPI.initGlobalModerationData();
            CoreDataAPI.initRankData();
            new PermissionCommands();

            // Initialize Core Module
            core = new CoreModule();
            ModuleManager.registerModule(core);

            // Initialize LuckyBlocks Module
            luckyBlocks = new LuckyBlocks();
            ModuleManager.registerModule(luckyBlocks);
        }
    }

    public static CorePlugin getPlugin() {
        return plugin;
    }

    public static BasicDAO<User, ObjectId> getUserDAO() {
        return plugin.getDAO(User.class);
    }

    public static BasicDAO<ModerationEntry, ObjectId> getModerationEntryDAO() {
        return plugin.getDAO(ModerationEntry.class);
    }

    public static BasicDAO<RankData, ObjectId> getRankDataDAO() {
        return plugin.getDAO(RankData.class);
    }
    
    public static BasicDAO<GlobalModerationData, ObjectId> getGlobalModerationDataDAO() {
        return plugin.getDAO(GlobalModerationData.class);
    }
    
    public static BasicDAO<Transaction, ObjectId> getTransactionDAO() {
        return plugin.getDAO(Transaction.class);
    }

    public static NetworkPlayer addPlayer(User user) {
        NetworkPlayer player;
        plugin.getPlayers().put(UUID.fromString(user.uuid), player = new NetworkPlayer(user));
        return player;
    }

    public static NetworkPlayer getPlayer(Player player) {
        return plugin.getPlayers().get(player.getUniqueId());
    }

    public static NetworkPlayer getPlayer(UUID uuid) {
        return plugin.getPlayers().get(uuid);
    }

    public static NetworkPlayer removePlayer(Player player) {
        return removePlayer(player.getUniqueId());
    }

    public static NetworkPlayer removePlayer(UUID uuid) {
        NetworkPlayer p = getPlayer(uuid);
        CorePlugin.getPlayers().remove(uuid);
        return p;
    }

    public static void async(Runnable runnable) {
        Async.execute(runnable);
    }

    public static <V> Future<V> async(Callable<V> callable) {
        return Async.submit(callable);
    }

    public static List<NetworkPlayer> getNetworkPlayers() {
        return new ArrayList<>(plugin.getPlayers().values());
    }

    public static boolean isLoaded() {
        return loaded;
    }

    public static void setLoaded(boolean loaded) {
        CoreAPI.loaded = loaded;
    }

    public static GlobalModerationData getGlobalModerationData() {
        return globalModerationData;
    }

    public static LuckyBlocks getLuckyBlocks() {
        return luckyBlocks;
    }

    public static boolean isPlayerLoaded(UUID uuid) {
        return getPlayer(uuid) != null;
    }

    public static boolean isPlayerLoaded(Player player) {
        return isPlayerLoaded(player.getUniqueId());
    }

    public static void debug(String ... messages) {
        for (String message : messages) {
            plugin.getLogger().info("[Debug] " + message);
        }
    }
}
