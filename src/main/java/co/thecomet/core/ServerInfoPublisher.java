package co.thecomet.core;

import co.thecomet.redis.bukkit.CRBukkit;
import co.thecomet.redis.redis.pubsub.NetTask;
import org.bukkit.scheduler.BukkitRunnable;

public class ServerInfoPublisher extends BukkitRunnable {
    public ServerInfoPublisher(CorePlugin plugin) {
        CRBukkit.getInstance().getRedis().registerChannel("serverUpdate");
        this.runTaskTimer(plugin, 20 * 2, 20 * 2);
    }

    @Override
    public void run() {
        send();
    }

    public void send() {
        NetTask.withName("serverUpdate")
                .withArg("id", CoreAPI.getPlugin().getId())
                .withArg("type", CoreAPI.getPlugin().getType())
                .withArg("status", CoreAPI.getPlugin().getStatus())
                .withArg("playersOnline", CoreAPI.getPlugin().getPlayersOnline())
                .withArg("maxPlayers", CoreAPI.getPlugin().getMaxPlayers())
                .withArg("vipOnly", CoreAPI.getPlugin().isVipOnly())
                .withArg("joinable", CoreAPI.getPlugin().isJoinable())
                .withArg("spectatorJoinInProgress", CoreAPI.getPlugin().isSpectatorJoinInProgress()).send("serverUpdate", CRBukkit.getInstance().getRedis());
    }
}
