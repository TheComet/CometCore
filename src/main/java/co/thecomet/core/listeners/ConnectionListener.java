package co.thecomet.core.listeners;

import co.thecomet.common.chat.FontColor;
import co.thecomet.common.user.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.db.CoreDataAPI;
import co.thecomet.core.db.entities.User;
import co.thecomet.core.events.LoginSuccessEvent;
import co.thecomet.core.permission.PermissionManager;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.redis.bukkit.CRBukkit;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ConnectionListener implements Listener {
    private static boolean announceJoin = true;

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerPreLogin(AsyncPlayerPreLoginEvent event) {
        if (CoreAPI.isLoaded() == false) {
            event.setKickMessage("This server is currently starting up!");
            event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
            return;
        }

        User user = CoreDataAPI.initUser(event.getUniqueId(), event.getName(), event.getAddress().getHostAddress());
        if (user == null) {
            event.setKickMessage("Sorry, something went wrong while initializing player data.");
            event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
            return;
        }

        user.lastServer = CRBukkit.getInstance().getDynamicSettings().getName();
        CoreDataAPI.setLastServer(event.getUniqueId(), user.lastServer);
        NetworkPlayer bp = CoreAPI.addPlayer(user);

        Bukkit.getScheduler().scheduleSyncDelayedTask(CoreAPI.getPlugin(), () -> Bukkit.getPluginManager().callEvent(new LoginSuccessEvent(bp)));
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        final NetworkPlayer bp = CoreAPI.getPlayer(player);

        if (bp == null) {
            event.getPlayer().kickPlayer("There was a problem loading your player data. Please inform the developers.");
            return;
        }

        if (bp.getRank().ordinal() >= Rank.VIP.ordinal() && bp.getRank().ordinal() <= Rank.ULTRA.ordinal()) {
            if (!bp.getRankCoinsReceived().contains(bp.getRank())) {
                bp.getRankCoinsReceived().add(bp.getRank());
                CoreDataAPI.updateRankCoinsReceived(bp.getUuid(), bp.getRank());
                CoreDataAPI.addCoins(bp.getRank().getRankPurchaseCoins(), bp.getUuid(), true);
            }
        }

        PermissionManager.update(bp.getUuid());
        if (event.getPlayer().getGameMode() != GameMode.SURVIVAL) {
            event.getPlayer().setGameMode(GameMode.SURVIVAL);
        }
        event.getPlayer().getInventory().setHeldItemSlot(0);

        if (announceJoin && bp.getRank().ordinal() >= Rank.ULTRA.ordinal()) {
            event.setJoinMessage(FontColor.translateString("&8[" + bp.getRank().getRankColor().toString() + bp.getRank().getDisplayName() + "&8] " + bp.getRank().getChatColor().toString() + bp.getName() + " &7has joined the server!"));
        } else {
            event.setJoinMessage(null);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onLoginSuccess(LoginSuccessEvent event) {
        NetworkPlayer bp = event.getProfile();

        if (bp.getModerationHistory().activeMute != null) {
            ChatListener.addMute(bp);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent event) {
        event.setQuitMessage(null);
        
        NetworkPlayer player = null;
        if ((player = CoreAPI.removePlayer(event.getPlayer())) == null) {
            return;
        }
        
        if (ChatListener.getMutedPlayers().containsKey(player.getUuid())) {
            ChatListener.getMutedPlayers().remove(player.getUuid());
        }

        CoreDataAPI.setTimeStats(player.getUuid(), System.currentTimeMillis() - player.getJoinServerTime(), System.currentTimeMillis());
        PermissionManager.remove(player.getUuid());
    }

    public static boolean isAnnounceJoin() {
        return announceJoin;
    }

    public static void setAnnounceJoin(boolean announceJoin) {
        ConnectionListener.announceJoin = announceJoin;
    }

    public static void kick(Player player, String message) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(CoreAPI.getPlugin(), () -> player.kickPlayer(message));
    }
}
