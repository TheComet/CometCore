package co.thecomet.core.utils;

import co.thecomet.redis.bukkit.CRBukkit;
import co.thecomet.redis.redis.pubsub.NetTask;
import org.bukkit.entity.Player;

public class NetworkUtil {
    public static void send(Player player, String server) {
        NetTask.withName("send")
                .withArg("player", player.getName())
                .withArg("server", server)
                .send("send", CRBukkit.getInstance().getRedis());
    }
}
