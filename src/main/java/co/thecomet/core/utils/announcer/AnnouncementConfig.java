package co.thecomet.core.utils.announcer;

import co.thecomet.common.config.JsonConfig;

import java.util.ArrayList;
import java.util.List;

public class AnnouncementConfig extends JsonConfig {
    public String[] header = new String[]{};
    public String[] footer = new String[]{};
    public String[] welcomeMessage = new String[]{};
    public List<String[]> announcements = new ArrayList<>();
}
