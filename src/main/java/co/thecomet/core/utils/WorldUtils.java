package co.thecomet.core.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public class WorldUtils {

    public static void clearAllEntities() {
        for (World world : Bukkit.getWorlds()) {
            clearWorldEntities(world);
        }
    }

    public static void clearWorldEntities(World world) {
        for (LivingEntity ent : world.getLivingEntities()) {
            if (ent instanceof Player) {
                continue;
            }
            ent.remove();
        }
    }

    public static Block getTargetBlock(Player player, int range) {
        Location loc = player.getEyeLocation();
        Vector dir = loc.getDirection().normalize();

        Block b = null;

        for (int i = 0; i <= range; i++) {
            b = loc.add(dir).getBlock();
        }

        return b;
    }
}
