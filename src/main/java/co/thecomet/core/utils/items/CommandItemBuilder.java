package co.thecomet.core.utils.items;

import co.thecomet.core.CoreAPI;
import co.thecomet.core.utils.ActionRunner;
import com.google.common.collect.Maps;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Map;

//TODO: Convert this class to a Listener instead of storing listeners
public class CommandItemBuilder {

    private final ItemStack item;
    private final Map<ListenerType, Listener> listeners = Maps.newEnumMap(ListenerType.class);

    private boolean registered = false;
    private boolean checkDura = false, checkLore = false;
    private ActionRunner actionRunner = null;
    private Action[] actions = Action.values();

    public CommandItemBuilder(ItemStack is) {
        this.item = is;
    }
    
    public static CommandItemBuilder start(ItemStack item) {
        CommandItemBuilder ci2 = new CommandItemBuilder(item);
                     ci2.listeners.put(ListenerType.INTERACT, ci2.buildInteract());
                     
        return ci2;
    }

    public CommandItemBuilder setActions(Action... act) {
        this.actions = act;
        return this;
    }
    
    public CommandItemBuilder setRunner(ActionRunner runner) {
        this.actionRunner = runner;
        return this;
    }
    
    public CommandItemBuilder setCheckDurability(boolean checkDurability) {
        this.checkDura = checkDurability;
        listeners.put(ListenerType.INTERACT, buildInteract());
        
        return this;
    }
    
    public CommandItemBuilder setCheckLore(boolean checkLore) {
        this.checkLore = checkLore;
        listeners.put(ListenerType.INTERACT, buildInteract());
        
        return this;
    }
    
    public CommandItemBuilder setCanDrop(final boolean canDrop) {
        if (!canDrop) {
            listeners.remove(ListenerType.DROP);
        } else {
            Listener l = new Listener() {
                @EventHandler
                public void onDropItem(PlayerDropItemEvent event) {
                    if (isSimilar(event.getItemDrop().getItemStack(), item, checkLore, checkDura)) {
                        event.setCancelled(true);
                    }
                }
            };
            
            listeners.put(ListenerType.DROP, l);
        }
        
        return this;
    }

    private Listener buildInteract() {
        if (registered) {
            return listeners.get(ListenerType.INTERACT);
        }

        return new Listener() {
            @EventHandler
            public void onInteract(PlayerInteractEvent event) {
                if (actionRunner == null) return;
                ItemStack handItem = event.getItem();
                if (handItem == null) return;
                
                for (Action action : actions) {
                    if (event.getAction() == action) {
                        if (isSimilar(handItem, item, checkLore, checkDura)) {
                            event.setCancelled(true);
                            actionRunner.action(event.getPlayer());
                            
                            if (actionRunner.willDestroy()) {
                                unregister();
                            }
                            
                            break;
                        }
                    }
                }
            }
        };
    }

    public void unregister() {
        registered = false;
        listeners.values().stream().forEach((list) -> HandlerList.unregisterAll(list));
    }

    // TODO: Check and make sure that this is valid
    public void register() {
        if (registered) return;
        registered = true;
        
        listeners.values().stream().forEach((list) -> Bukkit.getPluginManager().registerEvents(list, CoreAPI.getPlugin()));
    }
    
    static enum ListenerType {
        DROP, INTERACT;
    }

    private static boolean isSimilar(ItemStack one, ItemStack two, boolean checkLore, boolean checkDura) {
        boolean name = isSimilarName(one, two);
        boolean material = one.getType() == two.getType();
        boolean dura = one.getDurability() == two.getDurability();
        boolean lore = true;
        if (checkLore) {
            if (hasLore(one) && hasLore(two)) {
                if (hasLoreSize(one) && hasLoreSize(two)) {
                    if (getLoreSize(one) == getLoreSize(two)) {
                        int size = getLoreSize(one);
                        for (int i = 0; i < size; i++) {
                            String l1 = getLoreLine(one, i);
                            String l2 = getLoreLine(two, i);
                            if (!l1.equals(l2)) {
                                lore = false;
                                break;
                            }
                        }
                    } else {
                        lore = false;
                    }
                }
            }
        }
        return name && material && (checkDura ? dura : true) && lore;
    }

    private static boolean hasLore(ItemStack item) {
        if (item.hasItemMeta()) {
            if (item.getItemMeta().hasLore()) {
                return true;
            }
        }
        
        return false;
    }

    private static int getLoreSize(ItemStack item) {
        if (hasLore(item)) {
            return item.getItemMeta().getLore().size();
        }
        
        return 0;
    }

    private static boolean hasLoreSize(ItemStack item) {
        return hasLore(item) && getLoreSize(item) >= 1;
    }

    private static String getLoreLine(ItemStack item, int index) {
        try {
            return item.getItemMeta().getLore().get(index);
        } catch (Exception e) { }
        
        return "";
    }
    
    private static boolean isSimilarName(ItemStack one, ItemStack two) {
        try {
            String N1 = rip(one.getItemMeta().getDisplayName());
            String N2 = rip(two.getItemMeta().getDisplayName());
            return N1.equals(N2);
        } catch (Exception e) { }
        
        return false;
    }
    
    private static String rip(String str) {
        return ChatColor.stripColor(str);
    }
}
