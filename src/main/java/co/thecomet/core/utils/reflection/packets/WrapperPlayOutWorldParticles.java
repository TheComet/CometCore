package co.thecomet.core.utils.reflection.packets;

import co.thecomet.common.reflection.CommonReflection;
import co.thecomet.core.utils.reflection.VersionHandler;
import org.bukkit.Location;

import java.lang.reflect.Constructor;

public class WrapperPlayOutWorldParticles extends PacketWrapper {
    private static final Class<?> classPacketPlayOutWorldParticles = VersionHandler.getNMSClass("PacketPlayOutWorldParticles");
    private static final Class<?> classEnumParticle = VersionHandler.getNMSClass("EnumParticle");
    
    private final int id;
    private float x, y, z, dx, dy, dz, speed;
    private int amount;
    
    public WrapperPlayOutWorldParticles(int id) {
        super(classPacketPlayOutWorldParticles);
        this.id = id;
        this.x = 0f;
        this.y = 0f;
        this.z = 0f;
        this.dx = 0f;
        this.dy = 0f;
        this.dz = 0f;
        this.speed = 0f;
        this.amount = 1;
    }
    
    public WrapperPlayOutWorldParticles setLocation(Location location) {
        this.x = (float) location.getX();
        this.y = (float) location.getY();
        this.z = (float) location.getZ();
        return this;
    }
    
    public WrapperPlayOutWorldParticles setDeviations(float x, float y, float z) {
        this.dx = x;
        this.dy = y;
        this.dz = z;
        return this;
    }
    
    public WrapperPlayOutWorldParticles setSpeed(float speed) {
        this.speed = speed;
        return this;
    }
    
    public WrapperPlayOutWorldParticles setAmount(int amount) {
        this.amount = amount;
        return this;
    }
    
    public Object get() {
        Constructor<?> constructorPacketPlayOutWorldParticles = CommonReflection.getConstructor(classPacketPlayOutWorldParticles, new Class<?>[]{classEnumParticle, boolean.class, float.class, float.class, float.class, float.class, float.class, float.class, float.class, int.class, int[].class});
        return CommonReflection.constructNewInstance(constructorPacketPlayOutWorldParticles, new Object[] { classEnumParticle.getEnumConstants()[id], true, x, y, z, dx, dy, dz, speed, amount, new int[0]});
    }
}