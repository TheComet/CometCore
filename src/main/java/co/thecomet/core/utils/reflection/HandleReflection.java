package co.thecomet.core.utils.reflection;

import co.thecomet.common.reflection.CommonReflection;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.scoreboard.Objective;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class HandleReflection {
    public static Object getHandle(Entity entity) {
        Object nmsEntity = null;
        Method getHandle = CommonReflection.getMethod(entity.getClass(), "getHandle");

        try {
            nmsEntity = getHandle.invoke(entity);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return nmsEntity;
    }

    public static Object getHandle(World world) {
        Object nmsEntity = null;
        Method getHandle = CommonReflection.getMethod(world.getClass(), "getHandle");

        try {
            nmsEntity = getHandle.invoke(world);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return nmsEntity;
    }

    public static Object getHandle(Objective objective) {
        Class<?> classCraftObjective = VersionHandler.getOBCClass("scoreboard.CraftObjective");
        Method getHandle = CommonReflection.getMethod(classCraftObjective, "getHandle", 0);

        try {
            return getHandle.invoke(objective);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
