package co.thecomet.core.utils;

import co.thecomet.common.chat.FontColor;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public class MessageFormatter {
    private static String prefix = "\\u00BB";

    public static void sendUsageMessage(CommandSender sender, String... messages) {
        for (String message : messages) {
            sender.sendMessage(FontColor.translateString("&e&l" + prefix + "&6&l" + prefix + " &4&lUSAGE&7: &6" + message, true));
        }
    }

    public static void sendSuccessMessage(CommandSender sender, String... messages) {
        for (String message : messages) {
            sender.sendMessage(FontColor.translateString("&a&l" + prefix + "&2&l" + prefix + " &a&lSUCCESS&7: &a" + message, true));
        }
    }

    public static void sendErrorMessage(CommandSender sender, String... messages) {
        for (String message : messages) {
            sender.sendMessage(FontColor.translateString("&c&l" + prefix + "&4&l" + prefix + " &4&lERROR&7: &c" + message, true));
        }
    }

    public static void sendGeneralMessage(CommandSender sender, String... messages) {
        for (String message : messages) {
            sender.sendMessage(FontColor.translateString("&7&l" + prefix + "&8&l" + prefix + " &7" + message, true));
        }
    }

    public static void sendInfoMessage(CommandSender sender, String... messages) {
        for (String message : messages) {
            sender.sendMessage(FontColor.translateString("&e&l" + prefix + "&6&l" + prefix + " &6" + message, true));
        }
    }

    public static void sendAnnouncement(CommandSender sender, String... messages) {
        for (String message : messages) {
            sender.sendMessage(FontColor.translateString("&d&l" + prefix + "&5&l" + prefix + " &4&lANNOUNCEMENT&7: &d" + message, true));
        }
    }
}
