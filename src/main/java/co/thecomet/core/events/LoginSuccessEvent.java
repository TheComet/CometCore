package co.thecomet.core.events;

import co.thecomet.core.player.NetworkPlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class LoginSuccessEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private NetworkPlayer profile;

    public LoginSuccessEvent(NetworkPlayer profile) {
        this.profile = profile;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public NetworkPlayer getProfile() {
        return profile;
    }
}
