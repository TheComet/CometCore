package co.thecomet.core;

import co.thecomet.db.mongodb.ResourceManager;
import co.thecomet.core.command.CommandListener;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.core.ui.MenuAPI;
import co.thecomet.redis.bukkit.CRBukkit;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.dao.DAO;

import java.util.*;

/**
 * A base class that handles core feature initialization
 * and general plugin preparation.
 */
public abstract class CorePlugin extends JavaPlugin {
    private static MenuAPI menuAPI;
    private static Datastore datastore;
    private static Map<Class, DAO<?, ObjectId>> daoStore = new HashMap<>();
    private static Cache<UUID, NetworkPlayer> players = CacheBuilder.newBuilder().build();
    protected boolean vipOnly = false;
    protected boolean joinable = true;
    protected boolean spectatorJoinInProgress = true;

    public void onEnable() {
        // Init Command Listener
        CommandListener.setup(this);

        // Init Menu API
        if (menuAPI == null) {
            menuAPI = new MenuAPI(this);
        }

        // Init MongoDB Database
        initDatabase();

        // Init API
        new CoreAPI(this);

        // Enable
        enable();
        
        // VIP Only
        vipOnly = CoreModule.getCoreConfig().vipOnly;
        CRBukkit.setVipOnly(vipOnly);
        
        // Loaded
        CoreAPI.setLoaded(true);
    }

    public void onDisable() {
        // Disable
        disable();
    }

    /**
     * Enables a plugin
     */
    public abstract void enable();

    /**
     * Disables a plugin
     */
    public void disable() {}

    /**
     * Returns the server name specified in redis config.
     *
     * @return
     */
    public String getId() {
        return CRBukkit.getInstance().getDynamicSettings().getName();
    }

    /**
     * Returns the server type.
     * e.g. HUB, SG, FACTIONS, SW, etc...
     *
     * @return
     */
    public abstract String getType();

    /**
     * Returns the current status/phase of the server.
     * e.g. ONLINE, INPROGRESS, VOTING, etc...
     *
     * @return
     */
    public String getStatus() {
        return "ONLINE";
    }

    /**
     * Returns the number of players online on this server.
     *
     * @return
     */
    public long getPlayersOnline() {
        return Bukkit.getOnlinePlayers().size();
    }

    /**
     * Returns the max number of players allowed online.
     *
     * @return
     */
    public int getMaxPlayers() {
        return Bukkit.getMaxPlayers();
    }

    /**
     * Returns if the server is in vip mode.
     *
     * @return
     */
    public boolean isVipOnly() {
        return vipOnly;
    }

    /**
     * Sets the vipOnly state.
     *
     * @param vipOnly
     */
    public void setVipOnly(boolean vipOnly) {
        this.vipOnly = vipOnly;
    }

    public boolean isJoinable() {
        return joinable;
    }

    public void setJoinable(boolean joinable) {
        this.joinable = joinable;
    }

    public boolean isSpectatorJoinInProgress() {
        return spectatorJoinInProgress;
    }

    public void setSpectatorJoinInProgress(boolean spectatorJoinInProgress) {
        this.spectatorJoinInProgress = spectatorJoinInProgress;
    }

    /**
     * Initializes the database.
     *
     * @param
     */
    private void initDatabase() {
        List<Class<?>> classes = getDBClasses();
        if (datastore == null) {
            datastore = (classes == null || getDatabaseClasses().size() == 0) ? ResourceManager.getInstance().getDatastore() : ResourceManager.getInstance().getDatastore(new HashSet(getDatabaseClasses()));
        }

        if (classes != null && classes.size() != 0) {
            classes.forEach((Class<?> clazz) -> {
                registerDBClass(clazz);
            });
        }
    }

    /**
     * Registers a DAO for a database entity.
     *
     * @param clazz
     * @param
     */
    protected static void registerDBClass(Class<?> clazz) {
        BasicDAO<?, ObjectId> dao = new BasicDAO<>(clazz, datastore);
        datastore.ensureIndexes();
        datastore.ensureCaps();
        daoStore.put(clazz, dao);
    }

    /**
     * Registers DAOs for numerous database entities.
     *
     * @param classes
     */
    protected static void registerDBClasses(Class<?> ... classes) {
        for (Class clazz : classes) {
            registerDBClass(clazz);
        }
    }

    /**
     * Gets a list of database classes provided by a subclass.
     *
     * @param
     * @return
     */
    public List<Class<?>> getDBClasses() {
        return null;
    }

    /**
     * Retrieves the initialized datastore.
     *
     * @return
     */
    public static Datastore getDatastore() {
        return datastore;
    }

    /**
     * Retrieves a dao matching the specified class.
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> BasicDAO<T, ObjectId> getDAO(Class<T> clazz) {
        return (BasicDAO<T, ObjectId>) daoStore.get(clazz);
    }

    /**
     * Gets a list of all initialized players.
     *
     * @return
     */
    public static Map<UUID, NetworkPlayer> getPlayers() {
        return players.asMap();
    }

    protected static void addPlayers(NetworkPlayer player) {
        synchronized (players) {
            players.put(player.getUuid(), player);
        }
    }
}
