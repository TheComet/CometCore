package co.thecomet.core.permission;

import co.thecomet.common.async.Async;
import co.thecomet.common.user.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.db.entities.RankData;
import co.thecomet.core.player.NetworkPlayer;
import org.bukkit.Bukkit;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PermissionManager {
    private static Map<Rank, RankData> ranks = new HashMap<>();
    private static Map<UUID, PermissionEntry> permissions = new HashMap<>();
    
    public static void updateAll() {
        CoreAPI.getNetworkPlayers().forEach(player -> update(player.getUuid()));
    }

    public static void update(final UUID uuid) {
        Async.execute(() -> {
            NetworkPlayer bp;
            
            while ((bp = CoreAPI.getPlayer(uuid)) == null) {
                if (Bukkit.getPlayer(uuid) == null) {
                    return;
                }
            }
            
            final NetworkPlayer bpFinal = bp;
            Bukkit.getScheduler().scheduleSyncDelayedTask(CoreAPI.getPlugin(), () -> update(bpFinal));
        });
    }
    
    private static void update(final NetworkPlayer bp) {
        PermissionEntry entry = permissions.get(bp.getUuid());
        if (entry == null) {
            permissions.put(bp.getUuid(), (entry = new PermissionEntry(bp)));
        }

        final PermissionEntry finalEntry = entry;
        entry.attachment.getPermissions().keySet().forEach(entry.attachment::unsetPermission);
        Arrays.asList(Rank.values()).forEach((rank) -> {
            if (bp.getRank().ordinal() >= rank.ordinal()) {
                RankData data = ranks.get(rank);
                new HashMap<>(data.permissions).entrySet().forEach((e) -> {
                    String perm = e.getKey().replace("_", ".");
                    if (e.getValue() == false && finalEntry.attachment.getPermissions().containsKey(perm)) {
                        finalEntry.attachment.unsetPermission(perm);
                        return;
                    }

                    finalEntry.attachment.setPermission(perm, e.getValue());
                });
            }
        });
    }

    public static void remove(UUID uuid) {
        PermissionEntry entry = permissions.get(uuid);
        
        if (entry != null) {
            entry.attachment.remove();
            permissions.remove(uuid);
        }
    }

    public static Map<Rank, RankData> getRanks() {
        return ranks;
    }

    public static Map<UUID, PermissionEntry> getPermissions() {
        return new HashMap<>(permissions);
    }
}
