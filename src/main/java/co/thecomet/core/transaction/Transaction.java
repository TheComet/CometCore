package co.thecomet.core.transaction;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.UUID;

@Entity(value = "network_transactions", noClassnameStored = true)
public class Transaction {
    @Id
    public ObjectId id;
    public Feature feature;
    public String playerUuid;
    public transient boolean approved = false;
    
    public Transaction() {}
    
    public Transaction(Feature feature, UUID uuid) {
        this.feature =  feature;
        this.playerUuid = uuid.toString();
    }
}
