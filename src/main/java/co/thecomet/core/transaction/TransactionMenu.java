package co.thecomet.core.transaction;

import co.thecomet.common.chat.FontColor;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.db.entities.User;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.core.ui.Menu;
import co.thecomet.core.ui.MenuItem;
import co.thecomet.core.utils.MessageFormatter;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;
import org.mongodb.morphia.query.UpdateOperations;

public class TransactionMenu extends Menu {
    private Transaction transaction;
    private MenuItem approve;
    private MenuItem filler;
    private MenuItem cancel;
    
    public TransactionMenu(Player player, final Transaction transaction) {
        super("TRAN:" + TransactionManager.transactionSessionId++, 6);
        this.transaction = transaction;

        final NetworkPlayer bp = CoreAPI.getPlayer(player.getUniqueId());
        bp.getTransactions().add(transaction);

        this.setExitOnClickOutside(false);
        this.setMenuCloseBehavior(p -> {
            if (transaction.approved) {
                if (transaction.feature.type == CurrencyType.POINTS) {
                    bp.setPoints(bp.getPoints() - transaction.feature.points);
                } else {
                    bp.setPoints(bp.getCoins() - transaction.feature.points);
                }

                MessageFormatter.sendSuccessMessage(player, "Your purchase has been approved.");

                CoreAPI.async(() -> {
                    CoreAPI.getTransactionDAO().save(transaction);
                    UpdateOperations<User> ops = CoreAPI.getUserDAO().createUpdateOperations();
                    ops.add("transactions", transaction);
                    ops.inc(transaction.feature.type == CurrencyType.POINTS ? "points" : "coins", -transaction.feature.points);
                    CoreAPI.getUserDAO().update(CoreAPI.getUserDAO().createQuery().field("uuid").equal(bp.getUuid().toString()), ops);
                    MessageFormatter.sendSuccessMessage(player, "Your purchase has been completed!");
                });
            } else {
                bp.getTransactions().remove(transaction);
                MessageFormatter.sendGeneralMessage(player, "You have canceled the purchase.");
            }
        });
        
        approve = new MenuItem(FontColor.translateString("&aApprove Transaction"), new MaterialData(Material.STAINED_GLASS_PANE, DyeColor.LIME.getData())) {
            @Override
            public void onClick(Player player) {
                transaction.approved = true;
                closeMenu(player);
            }
        };

        filler = new MenuItem(FontColor.translateString("&a<-- Approve &7| &cCancel -->"), new MaterialData(Material.STAINED_GLASS_PANE, DyeColor.BLACK.getData())) {
            @Override
            public void onClick(Player player) {}
        };

        cancel = new MenuItem(FontColor.translateString("&cCancel Transaction"), new MaterialData(Material.STAINED_GLASS_PANE, DyeColor.RED.getData())) {
            @Override
            public void onClick(Player player) {
                closeMenu(player);
            }
        };
        
        for (int x = 0; x < 6; x++) {
            for (int y = 0; y < 9; y++) {
                if (y < 4) {
                    addMenuItem(approve, y, x);
                } else if (y > 4) {
                    addMenuItem(cancel, y, x);
                } else {
                    addMenuItem(filler, y, x);
                }
            }
        }
    }
}
