package co.thecomet.core.transaction;

public class Feature {
    public String display;
    public String id;
    public int points = 0;
    public boolean unique = true;
    public CurrencyType type = CurrencyType.POINTS;
    
    public Feature() {}
    
    public Feature(String display, String id, int points, boolean unique, CurrencyType type) {
        this(display, id, points, type);
        this.unique = unique;
    }

    public Feature(String display, String id, int points, CurrencyType type) {
        this(display, id, points);
        this.type = type;
    }

    public Feature(String display, String id, int points) {
        this.display = display;
        this.id = id;
        this.points = points;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Feature feature = (Feature) o;

        if (id != null ? !id.equals(feature.id) : feature.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = display != null ? display.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + points;
        result = 31 * result + (unique ? 1 : 0);
        return result;
    }
}
