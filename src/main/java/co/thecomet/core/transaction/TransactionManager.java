package co.thecomet.core.transaction;

import co.thecomet.core.CoreAPI;
import co.thecomet.core.db.entities.User;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.core.utils.MessageFormatter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class TransactionManager {
    protected static int transactionSessionId = 1;
    
    public static void submit(final Player player, final Feature feature) {
        final NetworkPlayer bp = CoreAPI.getPlayer(player);
        
        if (bp == null) {
            return;
        }

        CoreAPI.async(() -> {
            User user = CoreAPI.getUserDAO().findOne(CoreAPI.getUserDAO().createQuery().field("uuid").equal(bp.getUuid().toString()));
            
            if (user == null) {
                CoreAPI.getPlugin().getLogger().info("User is null...");
                return;
            }
            
            final int points = user.points;
            final List<Transaction> transactions = user.transactions;
            Bukkit.getScheduler().scheduleSyncDelayedTask(CoreAPI.getPlugin(), () -> {
                if (bp.getPoints() != points) {
                    bp.setPoints(points);
                }
                
                if (bp.getTransactions().containsAll(transactions) == false) {
                    bp.setTransactions(transactions);
                }
                
                if (feature.unique) {
                    new ArrayList<>(transactions).forEach(transaction -> {
                        if (transaction.feature.equals(feature)) {
                            MessageFormatter.sendGeneralMessage(player, "You have already purchased this feature!");
                            return;
                        }
                    });
                }

                if (feature.type == CurrencyType.POINTS) {
                    if (bp.getPoints() < feature.points) {
                        MessageFormatter.sendErrorMessage(player, "Insufficient points. You need " + feature.points + " to buy " + feature.display);
                        return;
                    }
                } else {
                    if (bp.getCoins() < feature.points) {
                        MessageFormatter.sendErrorMessage(player, "Insufficient coins. You need " + feature.points + " to buy " + feature.display);
                        return;
                    }
                }
                
                final Transaction transaction = new Transaction(feature, player.getUniqueId());
                TransactionMenu menu = new TransactionMenu(player, transaction);
                menu.openMenu(player);
            });
        });
    }
}
