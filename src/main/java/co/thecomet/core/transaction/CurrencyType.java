package co.thecomet.core.transaction;

import lombok.Getter;

public enum CurrencyType {
    POINTS("Points"),
    COINS("Coins");

    @Getter
    private String display;

    private CurrencyType(String display) {
        this.display = display;
    }
}
