package co.thecomet.core.moderation.commands;

import co.thecomet.common.user.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.core.db.CoreDataAPI;
import co.thecomet.core.db.entities.ModerationEntry;
import co.thecomet.core.db.entities.User;
import co.thecomet.core.db.entities.components.ModerationAction;
import co.thecomet.core.listeners.ChatListener;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.core.utils.TimeFormatUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MuteCommands {
    public MuteCommands() {
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "mute", Rank.FRIEND, MuteCommands::tempmute);
        CommandRegistry.registerPlayerCommand(CoreAPI.getPlugin(), "tempmute", Rank.FRIEND, MuteCommands::tempmute);
        CommandRegistry.registerUniversalCommand(CoreAPI.getPlugin(), "unmute", Rank.FRIEND, MuteCommands::unmute);
    }

    public static void tempmute(Player sender, String[] args) {
        if (args.length < 3) {
            MessageFormatter.sendUsageMessage(sender, "/tempmute <player> <#m|#h|#d> <reason>");
            return;
        }

        String player = args[0];
        String time = args[1];
        String reason = args[2];

        if (args.length > 3) {
            for (int i = 3; i < args.length; i++) {
                reason += " " + args[i];
            }
        }

        Player target = Bukkit.getPlayer(player);
        User user = target != null ? CoreDataAPI.retrieveUser(target.getUniqueId()) : CoreDataAPI.retrieveUserByName(player);

        if (user == null) {
            MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
            return;
        }

        long duration;
        try {
            duration = TimeFormatUtils.getTime(time).longValue();
            if (duration == -1) {
                MessageFormatter.sendUsageMessage(sender, "/tempmute <player> <#m|#h|#d> <reason>");
                return;
            }
        } catch (NumberFormatException e) {
            MessageFormatter.sendUsageMessage(sender, "/tempmute <player> <#m|#h|#d> <reason>");
            return;
        }

        ModerationEntry entry = new ModerationEntry(user.uuid.toString(), sender.getUniqueId().toString(), ModerationAction.MUTE, reason, duration);
        CoreDataAPI.addModerationEntry(entry);

        if (target != null) {
            NetworkPlayer bp = CoreAPI.getPlayer(target.getUniqueId());
            
            if (bp != null) {
                bp.getModerationHistory().activeMute = entry;
                ChatListener.addMute(bp);
            }
        }

        for (Player p : Bukkit.getOnlinePlayers()) {
            MessageFormatter.sendInfoMessage(p, player + " &cwas tempmuted by &e&l" + sender.getName(), "&cfor &6&l" + reason + " &7(" + time + ")");
        }
    }

    public static void unmute(CommandSender sender, String[] args) {
        if (args.length != 1) {
            MessageFormatter.sendUsageMessage(sender, "/unmute <player>");
            return;
        }

        String player = args[0];

        Player target = Bukkit.getPlayer(player);
        User user = target != null ? CoreDataAPI.retrieveUser(target.getUniqueId()) : CoreDataAPI.retrieveUserByName(player);

        if (user == null) {
            MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
            return;
        }

        if (target != null) {
            ChatListener.getMutedPlayers().remove(target.getUniqueId());
        }

        user.moderationHistory.activeMute = null;
        CoreAPI.getUserDAO().save(user);
        MessageFormatter.sendSuccessMessage(sender, ChatColor.GOLD + player + " &4has been unmuted.");
    }
}
