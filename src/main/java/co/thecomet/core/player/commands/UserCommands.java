package co.thecomet.core.player.commands;

import co.thecomet.common.user.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.command.CommandRegistry;
import co.thecomet.core.db.CoreDataAPI;
import co.thecomet.core.db.entities.User;
import co.thecomet.core.events.SetPlayerRankEvent;
import co.thecomet.core.permission.PermissionManager;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.core.utils.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class UserCommands {
    public UserCommands() {
        CommandRegistry.registerUniversalCommand(CoreAPI.getPlugin(), "setrank", Rank.ADMIN, UserCommands::setRank);
        CommandRegistry.registerUniversalCommand(CoreAPI.getPlugin(), "preloaduser", Rank.ADMIN, UserCommands::preLoadUser);
        CommandRegistry.registerUniversalCommand(CoreAPI.getPlugin(), "addpoints", Rank.ADMIN, UserCommands::addPoints);
        CommandRegistry.registerUniversalCommand(CoreAPI.getPlugin(), "addcoins", Rank.ADMIN, UserCommands::addCoins);
    }

    public static void setRank(CommandSender sender, String[] args) {
        if (args.length != 2) {
            MessageFormatter.sendUsageMessage(sender, "/setrank <player> <rank>");
            return;
        }

        String name = args[0];
        String rank = args[1];

        if (Rank.matchFound(rank) == false) {
            MessageFormatter.sendErrorMessage(sender, "That is not a valid rank.");
            return;
        }

        Rank r = Rank.getRankFromString(rank);
        Player player = Bukkit.getPlayer(name);
        NetworkPlayer bp = null;

        if (player != null) {
            bp = CoreAPI.getPlayer(player.getUniqueId());
        }

        User user = bp != null ? bp.getProfile() : CoreDataAPI.retrieveUserByName(name);

        if (user == null) {
            MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
            return;
        }

        user.rank = r;
        CoreAPI.getUserDAO().save(user);
        MessageFormatter.sendSuccessMessage(sender, name + "'s rank has been set to " + r.name());
        Bukkit.getPluginManager().callEvent(new SetPlayerRankEvent(user));

        if (bp != null) {
            bp.setRank(r);
            PermissionManager.update(bp.getUuid());
            MessageFormatter.sendInfoMessage(Bukkit.getPlayer(UUID.fromString(user.uuid)), "Your rank has been set to " + r.name());
        }
    }

    public static void preLoadUser(CommandSender sender, String[] args) {
        if (args.length != 1) {
            CoreAPI.debug("preload -> arguments not equal to 1");
            MessageFormatter.sendUsageMessage(sender, "/preloaduser <player>");
            return;
        }

        final String name = args[0];
        Player player = Bukkit.getPlayer(name);
        NetworkPlayer bp = null;
        if (player != null) {
            CoreAPI.debug("preload -> player is online");
            bp = CoreAPI.getPlayer(player.getUniqueId());
        }

        if (bp != null) {
            CoreAPI.debug("preload -> player already exists");
            MessageFormatter.sendErrorMessage(sender, "That user already exists in the database!");
            return;
        }

        CoreAPI.debug("preload -> retrieving user");
        User user = CoreDataAPI.retrieveUserByName(name);
        if (user != null) {
            CoreAPI.debug("preload -> user already exists");
            MessageFormatter.sendErrorMessage(sender, "That user already exists in the database!");
            return;
        }

        CoreAPI.async(() -> {
            List<String> names = new ArrayList<String>() {{ add(name); }};
            UUIDFetcher fetcher = new UUIDFetcher(names);
            Map<String, UUID> uniqueIds = null;

            try {
                uniqueIds = fetcher.call();
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            if (uniqueIds == null || uniqueIds.isEmpty()) {
                CoreAPI.debug("preload -> no user found: " + name);
                MessageFormatter.sendErrorMessage(sender, "No player could be found using the name " + name);
                return;
            }

            for (Map.Entry<String, UUID> entry : uniqueIds.entrySet()) {
                if (entry.getKey().equalsIgnoreCase(name)) {
                    if (entry.getValue() == null) {
                        return;
                    }

                    CoreAPI.debug("preload -> matched username: " + entry.getKey());
                    CoreAPI.debug("preload -> matched uuid: " + entry.getValue().toString());
                    CoreDataAPI.initUser(entry.getValue(), entry.getKey(), null);
                    MessageFormatter.sendSuccessMessage(sender, "The player has successfully been preloaded into the database.");
                    return;
                }
            }

            MessageFormatter.sendErrorMessage(sender, "We were unable to add that user to the database.");
        });
    }
    
    public static void addPoints(CommandSender sender, String[] args) {
        if (args.length != 2) {
            MessageFormatter.sendUsageMessage(sender, "/addpoints <name> <points>");
            return;
        }
        
        String name = args[0];
        final int points;
        try {
            points = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            MessageFormatter.sendUsageMessage(sender, "/addpoints <name> <points>");
            return;
        }

        final Player player = Bukkit.getPlayer(name);
        CoreAPI.async(() -> {
            NetworkPlayer bp = null;

            if (player != null) {
                bp = CoreAPI.getPlayer(player.getUniqueId());
            }

            User user = bp != null ? bp.getProfile() : CoreDataAPI.retrieveUserByName(name);

            if (user == null) {
                MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
                return;
            }

            CoreDataAPI.addPoints(points, UUID.fromString(user.uuid), true);
            MessageFormatter.sendSuccessMessage(sender, user.name + " has been given " + points + " points!");
        });
    }

    public static void addCoins(CommandSender sender, String[] args) {
        if (args.length != 2) {
            MessageFormatter.sendUsageMessage(sender, "/addcoins <name> <coins>");
            return;
        }

        String name = args[0];
        final int coins;
        try {
            coins = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            MessageFormatter.sendUsageMessage(sender, "/addcoins <name> <coins>");
            return;
        }

        final Player player = Bukkit.getPlayer(name);
        CoreAPI.async(() -> {
            NetworkPlayer bp = null;

            if (player != null) {
                bp = CoreAPI.getPlayer(player.getUniqueId());
            }

            User user = bp != null ? bp.getProfile() : CoreDataAPI.retrieveUserByName(name);

            if (user == null) {
                MessageFormatter.sendErrorMessage(sender, "Unable to find user in the database. Is the name correct?");
                return;
            }

            CoreDataAPI.addCoins(coins, UUID.fromString(user.uuid), true);
            MessageFormatter.sendSuccessMessage(sender, user.name + " has been given " + coins + " coins!");
        });
    }
}
