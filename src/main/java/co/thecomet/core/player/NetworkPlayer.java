package co.thecomet.core.player;

import co.thecomet.common.user.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.db.entities.User;
import co.thecomet.core.db.entities.components.ModerationHistory;
import co.thecomet.core.transaction.Transaction;
import lombok.Getter;
import lombok.Setter;

import java.util.*;

public class NetworkPlayer {
    @Getter
    private UUID uuid;
    @Getter @Setter
    private String name;
    @Getter @Setter
    private Rank rank;
    @Getter @Setter
    private String ip;
    @Getter
    private List<String> ipHistory;
    @Getter @Setter
    private long joinServerTime;
    @Getter @Setter
    private long playTime;
    @Getter @Setter
    private int points;
    @Getter @Setter
    private int coins;
    @Getter
    private ModerationHistory moderationHistory;
    @Getter
    private Map<Class<?>, Object> data = new HashMap<Class<?>, Object>();
    @Getter @Setter
    private List<Transaction> transactions = new ArrayList<>();
    @Getter @Setter
    private List<Rank> rankCoinsReceived;

    public NetworkPlayer(User user) {
        this.uuid = UUID.fromString(user.uuid);
        this.name = user.name;
        this.rank = user.rank;
        this.ip = user.ip;
        this.ipHistory = user.ipHistory;
        this.playTime = user.playTime;
        this.joinServerTime = System.currentTimeMillis();
        this.points = user.points;
        this.coins = user.coins;
        this.moderationHistory = user.moderationHistory;
        this.transactions = user.transactions;
        this.rankCoinsReceived = user.rankCoinsReceived;
    }

    /**
     * Retrieves a player's profile from the database.
     * This method should be used sparingly and only when needed.
     *
     * @return
     */
    public User getProfile() {
        return CoreAPI.getUserDAO().findOne("uuid", uuid.toString());
    }

    public <T> T getData(Class<T> clazz) {
        Object object = data.get(clazz);
        if (object == null || !clazz.isInstance(object)) {
            return null;
        }
        return clazz.cast(object);
    }

    public void setData(Class<?> clazz, Object data) {
        this.data.put(clazz, data);
    }
}
