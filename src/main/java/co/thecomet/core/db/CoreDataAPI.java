package co.thecomet.core.db;

import co.thecomet.common.user.Rank;
import co.thecomet.core.CoreAPI;
import co.thecomet.core.db.entities.GlobalModerationData;
import co.thecomet.core.db.entities.ModerationEntry;
import co.thecomet.core.db.entities.RankData;
import co.thecomet.core.db.entities.User;
import co.thecomet.core.db.entities.components.ModerationAction;
import co.thecomet.core.permission.PermissionManager;
import co.thecomet.core.player.NetworkPlayer;
import co.thecomet.core.utils.MessageFormatter;
import co.thecomet.core.utils.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.mongodb.morphia.query.UpdateOperations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class CoreDataAPI {
    /**
     * Initializes a network user instance for an online player.
     *
     * @param name the name of the user
     * @param uuid the uuid of the user
     * @param ip the ip of the user
     * @return Networkuser instance
     */
    public static User initUser(UUID uuid, String name, String ip) {
        CoreAPI.debug("initUser -> retrieving");
        User user = retrieveUser(uuid);

        if (user == null) {
            CoreAPI.debug("initUser -> not found, creating");
            user = new User(uuid.toString());
        }

        CoreAPI.debug("initUser -> updating user");
        updateUser(user, name, ip);

        return user;
    }

    /**
     * Updates the core data of a user.
     *
     * @param user
     * @param name
     * @param ip
     */
    public static void updateUser(User user, String name, String ip) {
        boolean updated = false;
        if (user.name == null || !(user.name.equalsIgnoreCase(name)) || user.nameLower == null || !(user.nameLower == name.toLowerCase())) {
            if (user.name != null && !user.nameHistory.contains(user.name)) {
                user.nameHistory.add(user.name);
            }

            user.name = name;
            user.nameLower = name.toLowerCase();
            updated = true;
        }

        if (user.ip == null || user.ip.equalsIgnoreCase(ip) == false) {
            if (user.ip != null && !user.ipHistory.contains(user.ip)) {
                user.ipHistory.add(user.ip);
            }

            user.ip = ip;
            updated = true;
        }

        if (updated) {
            CoreAPI.async(() -> {
                if (user.id == null) {
                    CoreAPI.debug("updateUser -> saving user for first time");
                    CoreAPI.getUserDAO().save(user);
                } else {
                    CoreAPI.debug("updateUser -> updating user name and ip data");
                    UpdateOperations<User> ops = CoreAPI.getUserDAO().createUpdateOperations();
                    ops.set("name", name);
                    if (ip != null) {
                        ops.set("ip", ip);
                    }
                    ops.set("nameHistory", user.nameHistory);
                    if (ip != null) {
                        ops.set("ipHistory", user.ipHistory);
                    }
                    CoreAPI.getUserDAO().update(CoreAPI.getUserDAO().createQuery().field("uuid").equal(user.uuid), ops);
                }
            });
        }
    }

    /**
     * Retrieves a profile matching the specified uuid.
     *
     * @param uuid
     * @return
     */
    public static User retrieveUser(UUID uuid) {
        return CoreAPI.getUserDAO().findOne("uuid", uuid.toString());
    }

    /**
     * Retrieves a profile matching the specified name.
     *
     * @param name
     * @return
     */
    public static User retrieveUserByName(String name) {
        return CoreAPI.getUserDAO().findOne("nameLower", name.toLowerCase());
    }

    /**
     * Adds a moderation entry to a user's moderation history.
     *
     * @param entry
     */
    public static void addModerationEntry(ModerationEntry entry) {
        CoreAPI.async(() -> {
            CoreAPI.getModerationEntryDAO().save(entry);

            UpdateOperations<User> ops = CoreAPI.getUserDAO().createUpdateOperations();
            ops.add("moderationHistory.entries", entry);

            if (entry.action == ModerationAction.BAN) {
                ops.set("moderationHistory.activeBan", entry);
            } else if (entry.action  == ModerationAction.MUTE) {
                ops.set("moderationHistory.activeMute", entry);
            }

            CoreAPI.getUserDAO().update(CoreAPI.getUserDAO().createQuery().field("uuid").equal(entry.uuid), ops);
        });
    }

    /**
     * Sets a user's play time and/or moment last online.
     *
     * @param uuid
     * @param playTime
     * @param lastOnline
     */
    public static void setTimeStats(UUID uuid, long playTime, long lastOnline) {
        CoreAPI.async(() -> {
            UpdateOperations<User> operations = CoreAPI.getUserDAO().createUpdateOperations();
            operations.inc("playTime", playTime);
            operations.set("lastOnline", lastOnline);
            CoreAPI.getUserDAO().update(CoreAPI.getUserDAO().createQuery().field("uuid").equal(uuid.toString()), operations);
        });
    }

    /**
     * Sets the user's last server they were seen on.
     *
     * @param uuid
     * @param name
     */
    public static void setLastServer(UUID uuid, String name) {
        CoreAPI.async(() -> {
            UpdateOperations<User> operations = CoreAPI.getUserDAO().createUpdateOperations();
            operations.set("lastServer", name);
            CoreAPI.getUserDAO().update(CoreAPI.getUserDAO().createQuery().field("uuid").equal(uuid.toString()), operations);
        });
    }

    /**
     * Initializes RankData for all rank enums and
     * stores them in the permissions manager.
     */
    public static void initRankData() {
        List<Rank> ranks = new ArrayList<>(Arrays.asList(Rank.values()));
        for (RankData data : CoreAPI.getRankDataDAO().find().asList()) {
            if (ranks.contains(data.rank)) {
                PermissionManager.getRanks().put(data.rank, data);
                ranks.remove(data.rank);
            }
        }

        for (Rank rank : ranks) {
            RankData data = new RankData(rank);
            CoreAPI.getRankDataDAO().save(data);
            PermissionManager.getRanks().put(rank, data);
        }
    }

    public static GlobalModerationData initGlobalModerationData() {
        GlobalModerationData data = CoreAPI.getGlobalModerationDataDAO().find().get();
        if (data == null) {
            data = new GlobalModerationData();
            CoreAPI.getGlobalModerationDataDAO().save(data);
        }
        return data;
    }
    
    public static void banIps(List<String> ips) {
        CoreAPI.async(() -> {
            UpdateOperations<GlobalModerationData> ops = CoreAPI.getGlobalModerationDataDAO().createUpdateOperations();
            ops.addAll("ipBans", ips, false);
            CoreAPI.getGlobalModerationDataDAO().update(CoreAPI.getGlobalModerationDataDAO().createQuery(), ops);
        });
    }
    
    public static void unbanIps(List<String> ips) {
        CoreAPI.async(() -> {
            UpdateOperations<GlobalModerationData> ops = CoreAPI.getGlobalModerationDataDAO().createUpdateOperations();
            ops.removeAll("ipBans", ips);
            CoreAPI.getGlobalModerationDataDAO().update(CoreAPI.getGlobalModerationDataDAO().createQuery(), ops);
        });
    }

    public static void addPoints(int points, Player player, boolean inform) {
        addPointsInternal(points, player.getUniqueId(), inform, true);
    }

    public static void addPoints(int points, String name, boolean inform) {
        Player player = Bukkit.getPlayer(name);
        if (player != null) {
            addPoints(points, player, inform);
        } else {
            CoreAPI.async(() -> {
                try {
                    UUID uuid = UUIDFetcher.getUUIDOf(name);
                    if (uuid != null) {
                        addPointsInternal(points, uuid, inform);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public static void addPoints(int points, UUID uuid, boolean inform) {
        addPointsInternal(points, uuid, inform, true);
    }

    private static void addPointsInternal(int points, UUID uuid, boolean inform, boolean async) {
        if (async) {
            CoreAPI.async(() -> {
                addPointsInternal(points, uuid, inform);
            });
        } else {
            addPointsInternal(points, uuid, inform);
        }
    }

    private static void addPointsInternal(int points, UUID uuid, boolean inform) {
        UpdateOperations<User> ops = CoreAPI.getUserDAO().createUpdateOperations();
        ops.inc("points", points);
        CoreAPI.getUserDAO().update(CoreAPI.getUserDAO().createQuery().field("uuid").equal(uuid.toString()), ops);

        NetworkPlayer bp = CoreAPI.getPlayer(uuid);
        if (bp != null) {
            bp.setPoints(bp.getPoints() + points);
            if (inform) {
                Player player = Bukkit.getPlayer(uuid);
                if (player != null) {
                    MessageFormatter.sendInfoMessage(player, "You have been given " + points + " points!", "Your balance is now " + bp.getPoints() + "!");
                }
            }
        }
    }

    /**
     * Adds points to a user profile.
     *
     * @param points
     * @param user
     * @param inform
     */
    public static void addPoints(int points, User user, boolean inform) {
        if (user == null) {
            return;
        }

        addPoints(points, UUID.fromString(user.uuid), inform);
    }

    // COIN DATA MANIPULATION

    public static void addCoins(int coins, Player player, boolean inform) {
        addCoinsInternal(coins, player.getUniqueId(), inform, true);
    }

    public static void addCoins(int coins, String name, boolean inform) {
        Player player = Bukkit.getPlayer(name);
        if (player != null) {
            addCoins(coins, player, inform);
        } else {
            CoreAPI.async(() -> {
                try {
                    UUID uuid = UUIDFetcher.getUUIDOf(name);
                    if (uuid != null) {
                        addCoinsInternal(coins, uuid, inform);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public static void addCoins(int coins, UUID uuid, boolean inform) {
        addCoinsInternal(coins, uuid, inform, true);
    }

    private static void addCoinsInternal(int coins, UUID uuid, boolean inform, boolean async) {
        if (async) {
            CoreAPI.async(() -> {
                addCoinsInternal(coins, uuid, inform);
            });
        } else {
            addCoinsInternal(coins, uuid, inform);
        }
    }

    private static void addCoinsInternal(int coins, UUID uuid, boolean inform) {
        UpdateOperations<User> ops = CoreAPI.getUserDAO().createUpdateOperations();
        ops.inc("coins", coins);
        CoreAPI.getUserDAO().update(CoreAPI.getUserDAO().createQuery().field("uuid").equal(uuid.toString()), ops);

        NetworkPlayer bp = CoreAPI.getPlayer(uuid);
        if (bp != null) {
            bp.setCoins(bp.getCoins() + coins);
            if (inform) {
                Player player = Bukkit.getPlayer(uuid);
                if (player != null) {
                    MessageFormatter.sendInfoMessage(player, "You have been given " + coins + " coins!", "Your balance is now " + bp.getCoins() + "!");
                }
            }
        }
    }

    /**
     * Adds coins to a user profile.
     *
     * @param coins
     * @param user
     * @param inform
     */
    public static void addCoins(int coins, User user, boolean inform) {
        if (user == null) {
            return;
        }

        addCoins(coins, UUID.fromString(user.uuid), inform);
    }

    public static void updateRankCoinsReceived(UUID uuid, Rank rank) {
        CoreAPI.async(() -> {
            UpdateOperations<User> ops = CoreAPI.getUserDAO().createUpdateOperations();
            ops.add("rankCoinsReceived", rank);
            CoreAPI.getUserDAO().update(CoreAPI.getUserDAO().createQuery().field("uuid").equal(uuid.toString()), ops);
        });
    }
}