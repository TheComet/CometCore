package co.thecomet.core.db.entities.components;

public enum ModerationAction {
    BAN,
    IPBAN,
    KICK,
    MUTE;
}
