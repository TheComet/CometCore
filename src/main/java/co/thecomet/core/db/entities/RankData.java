package co.thecomet.core.db.entities;

import co.thecomet.common.user.Rank;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.HashMap;
import java.util.Map;

@Entity(value = "network_rank_data", noClassnameStored = true)
public class RankData {
    @Id
    public ObjectId id;
    public Rank rank;
    public Map<String, Boolean> permissions = new HashMap<>();

    public RankData(Rank rank) {
        this.rank = rank;
    }

    public RankData() {}
}
