package co.thecomet.core.db.entities.components;

import co.thecomet.core.db.entities.ModerationEntry;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Reference;

import java.util.ArrayList;
import java.util.List;

@Embedded
public class ModerationHistory {
    @Reference
    public List<ModerationEntry> entries = new ArrayList<>();
    @Reference
    public ModerationEntry activeBan;
    @Reference
    public ModerationEntry activeMute;

    public ModerationHistory() {}
}
