package co.thecomet.core.db.entities;

import co.thecomet.common.user.Rank;
import co.thecomet.core.db.entities.components.ModerationHistory;
import co.thecomet.core.transaction.Transaction;
import com.google.common.collect.Lists;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;

import java.util.ArrayList;
import java.util.List;

@Entity(value = "network_users", noClassnameStored = true)
public class User {
    @Id
    public ObjectId id;
    @Indexed(unique = true)
    public String uuid;
    public Rank rank = Rank.DEFAULT;
    @Indexed
    public String name;
    @Indexed
    public String nameLower;
    public List<String> nameHistory = new ArrayList<>();
    public String ip;
    public List<String> ipHistory = new ArrayList<>();
    public long playTime = 0;
    public long lastOnline = 0;
    public String lastServer = "";
    public ModerationHistory moderationHistory = new ModerationHistory();
    public int points = 0;
    public int coins = 0;
    public List<Transaction> transactions = new ArrayList<>();
    public List<Rank> rankCoinsReceived = Lists.newArrayList();

    public User(String uuid) {
        this.uuid = uuid;
    }

    public User() {}
}
