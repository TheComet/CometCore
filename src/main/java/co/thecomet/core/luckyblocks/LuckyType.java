package co.thecomet.core.luckyblocks;

public enum LuckyType {
    /**
     * Any rank can activate this LuckyBlock
     */
    NORMAL,

    /**
     * Only VIPs and up can activate this LuckyBlock
     */
    VIP,

    /**
     * Any rank can activate this LuckyBlock
     */
    BOTH;
}
