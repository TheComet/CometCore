package co.thecomet.core.ui;

import org.bukkit.entity.Player;

public interface MenuCloseBehavior {
    public void onClose(Player player);
}
